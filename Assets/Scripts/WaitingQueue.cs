﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WaitingQueue : MonoBehaviour
{
    [SerializeField] protected List<QueuePosition> queuePositionTable;

    private float timeSinceLastQueueUpdate = 0.0f;
    private float queueUpdateInterval = 1.0f;
    //private DragObjects dragObjectManager;

    private void Start()
    {
        Initialise();

        //dragObjectManager = GameManager.GetGameManager().GetDragObjectManager();
    }

    protected virtual void Initialise()
    {
        int loc_queue_position_index = 0;

        foreach ( QueuePosition loc_queue_position in queuePositionTable )
        {
            loc_queue_position.SetWaitingQueue( this, loc_queue_position_index );
            ++loc_queue_position_index;
        }
    }

    protected void UpdateQueue()
    {
        timeSinceLastQueueUpdate += Time.deltaTime;

        if ( timeSinceLastQueueUpdate >= queueUpdateInterval )
        {
            ConcreteQueueUpdate();
            timeSinceLastQueueUpdate = 0;
        }
    }

    protected abstract void ConcreteQueueUpdate();
    //public DragObjects GetDragObjectManager()
    //{
    //    return dragObjectManager;
    //}

    public void FillPosition( Character par_dragged_character, QueuePosition par_queue_position )
    {
        if ( par_queue_position.GetCurrentCharacter() != null )
        {
            Character loc_next_character = null;

            foreach ( QueuePosition loc_queue_position in queuePositionTable )
            {
                if ( loc_queue_position == par_queue_position )
                {
                    loc_next_character = par_dragged_character;
                }

                if ( loc_next_character != null )
                {
                    Character loc_current_character = loc_queue_position.GetCurrentCharacter();

                    if ( loc_current_character != null )
                    {
                        loc_current_character.SetQueuePosition( null );
                    }

                    loc_queue_position.SetCurrentCharacter( loc_next_character );

                    loc_next_character = loc_current_character;
                }
            }
        }
        else
        {
            par_queue_position.SetCurrentCharacter( par_dragged_character );
        }
    }
    
    protected void AdvanceInQueueIfPossibleCoroutine()
    {
        List<QueuePosition> loc_free_position_table = new List<QueuePosition>();

        foreach (QueuePosition loc_queue_position in queuePositionTable)
        {
            Character loc_current_character = loc_queue_position.GetCurrentCharacter();

            if (loc_current_character == null)
            {
                loc_free_position_table.Add(loc_queue_position);
            }
            else if (loc_free_position_table.Count > 0)
            {
                if (loc_current_character.HasReachDestination())
                {
                    QueuePosition loc_new_position = loc_free_position_table[loc_free_position_table.Count - 1];

                    loc_free_position_table.RemoveAt(loc_free_position_table.Count - 1);
                    loc_current_character.ExitCurrentQueue();
                    loc_new_position.SetCurrentCharacter(loc_current_character);
                }
                loc_free_position_table.Clear();
            }
        }
    }

    public int GetEmptySpaceCount()
    {
        int loc_empty_space_count = 0;

        foreach ( QueuePosition loc_queue_position in queuePositionTable )
        {
            if ( loc_queue_position.GetCurrentCharacter() == null )
            {
                ++loc_empty_space_count;
            }
        }

        return loc_empty_space_count;
    }

    public bool IsSpaceAvailable( QueuePosition par_queue_position )
    {
        bool loc_has_found_space = false;
        int loc_free_space_behind_count = 0;
        
        foreach ( QueuePosition loc_queue_position in queuePositionTable )
        {
            if ( loc_queue_position == par_queue_position )
            {
                loc_has_found_space = true;
            }

            if ( loc_has_found_space )
            {
                if ( loc_queue_position.GetCurrentCharacter() == null )
                {
                    ++loc_free_space_behind_count;
                }
            }
        }

        return loc_free_space_behind_count > 0;
    }
}