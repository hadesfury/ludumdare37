﻿using System.Collections.Generic;

public class Illness
{
    public enum IllnessType
    {
        SHRINKNESS,
        BALDNESS,
        ARMS_LACK
    }

    public IllnessType illnessType;
    public string illnessName;
    public float healthSpeed;
    public float moodSpeed;
    public int healedScore;
    public int dieScore;
    public float treatementLength;

    public static readonly Dictionary<IllnessType, Illness> ILLNESS_TABLE = new Dictionary<IllnessType, Illness>()
    {
        {
            IllnessType.SHRINKNESS, new Illness()
            {
                illnessType = IllnessType.SHRINKNESS,
                illnessName = "Shrinkness",
                healthSpeed = 0.02f,
				moodSpeed = 0.02f,
                healedScore = 100,
                dieScore = 500,
                treatementLength = 5.0f
            }
        },
        {
            IllnessType.BALDNESS, new Illness()
            {
                illnessType = IllnessType.BALDNESS,
                illnessName = "Baldness",
				healthSpeed = 0.001f,
				moodSpeed = 0.030f,
                healedScore = 100,
                dieScore = 500,
                treatementLength = 5.0f
            }
        },
        {
            IllnessType.ARMS_LACK, new Illness()
            {
                illnessType = IllnessType.ARMS_LACK,
                illnessName = "Arms lack",
				healthSpeed = 0.045f,
				moodSpeed = 0.005f,
                healedScore = 100,
                dieScore = 500,
                treatementLength = 5.0f
            }
        }
    };
}