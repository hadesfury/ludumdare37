﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class DoctorQueue : WaitingQueue
{
    [SerializeField] private MeshRenderer availabilityLightBulb;
    [SerializeField] private Color availableColor = Color.green;
    [SerializeField] private Color disableColor = Color.red;
    [SerializeField] private Transform doctorOfficeDoor;

    private float elapsedTimeSinceLastEntering = 0.0f;
    private bool isWaitingForNextCharacter = false;
    private Character currentTreatedCharacter = null;
    private Light light;


    protected override void Initialise()
    {
        base.Initialise();

        GameManager.GetGameManager().Register( this );
        light = GetComponentInChildren<Light>();

    }

    private void Update()
    {
        elapsedTimeSinceLastEntering += Time.deltaTime;

        UpdateQueue();
        
        {
            isWaitingForNextCharacter = true;

            if ( currentTreatedCharacter != null )
            {
                float loc_treatment_time = Illness.ILLNESS_TABLE[ currentTreatedCharacter.GetIllnessType() ].treatementLength;

                if ( elapsedTimeSinceLastEntering < loc_treatment_time )
                {
                    isWaitingForNextCharacter = false;
                }
                else
                {
                    currentTreatedCharacter = null;
                }
            }
        }

        availabilityLightBulb.material.color = isWaitingForNextCharacter
            ? availableColor
            : disableColor;
        availabilityLightBulb.material.SetColor("_EmissionColor", availabilityLightBulb.material.color);
        if(light) light.color = availabilityLightBulb.material.color;
    }

    protected override void ConcreteQueueUpdate()
    {
        AdvanceInQueueIfPossibleCoroutine();

        if ( isWaitingForNextCharacter )
        {
            QueuePosition loc_queue_position = queuePositionTable[ 0 ];
            Character loc_next_character_to_enter = loc_queue_position.GetCurrentCharacter();

            if ( ( loc_next_character_to_enter != null )
                 && loc_next_character_to_enter.HasReachDestination() )
            {
                DOTween.Sequence()
                    .Append( doctorOfficeDoor.DOLocalRotate( new Vector3( 0, 0, 90 ), 1.0f ) )
                    .AppendInterval( 1.0f )
                    .Append( doctorOfficeDoor.DOLocalRotate( new Vector3( 0, 0, 0 ), 1.0f ) );
                
                loc_next_character_to_enter.GoToDoctorOffice();
                elapsedTimeSinceLastEntering = 0;
                currentTreatedCharacter = loc_next_character_to_enter;
                isWaitingForNextCharacter = false;
            }
        }
    }
}