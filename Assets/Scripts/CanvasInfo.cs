﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasInfo : MonoBehaviour
{
    [SerializeField] private Text infoText;

    private void Start()
    {
        transform.position += Vector3.up * 1.8f;
        DOTween.Sequence()
            .Append( transform.DOMoveY( 3.0f, 2.0f ).SetEase( Ease.OutElastic ))
            .Append( GetComponent<CanvasGroup>().DOFade( 0, 1.0f ))
            .AppendCallback( () => GameObject.Destroy( gameObject ) );
    }

    private void Update()
    {
        transform.LookAt( GameManager.GetGameManager().GetCamera().transform.position );
    }

    public void SetText( string par_text )
    {
        infoText.text = par_text;
    }
}