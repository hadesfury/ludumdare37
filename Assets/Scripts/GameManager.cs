﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header( "Prefabs" )] //
    [SerializeField] private CanvasInfo positiveCanvasInfoPrefab;
    [SerializeField] private CanvasInfo negativeCanvasInfoPrefab;

    [Header( "Ui" )] //
    [SerializeField] private IngameGuiLayout ingameGuiLayout;
    [SerializeField] private MenuGuiLayout menuGuiLayout;
    [SerializeField] private Texture2D waitingCursorTexture;
    [SerializeField] private RectTransform titleScreen;

    [Header( "Misc" )] //
    [SerializeField] private Camera currentCamera;
    [SerializeField] private CharacterSpawner characterSpawner;
    [SerializeField] private Transform hospitalExit;
    [SerializeField] private Transform doctorOffice;
    [Header( "Sounds" )] //
    [SerializeField] private AudioSource musicAudioSource;
    [SerializeField] private List<AudioClip> musicAudioClipTable;

    private static GameManager gameManager;
    //private DragObjects dragObjectManager;
    private Character selectedCharacter;
    private readonly List<WaitingQueue> waitingQueueTable = new List<WaitingQueue>();
    private int currentWaveIndex;
    private int currentWaveCount;
    private DayStat dayStat;
    private readonly List<DayStat> archivedDayStatTable = new List<DayStat>();
    private int currentClipIndex;

    private GameState currentGameState = GameState.NONE;

    public enum GameState
    {
        MAIN_MENU,
        INGAME,
        SCORE_SCREEN,
        GAME_OVER,
        NONE
    }

    public enum StateMessage
    {
        ANY_KEY_DOWN,
        GAME_OVER
    }

    public class DayStat
    {
        public int healedCount = 0;
        public int diedCount = 0;
        public int leaverCount = 0;
        public int moneyEarned = 0;
        public int moneyLost = 0;
    }

    private void Awake()
    {
        gameManager = this;
        //dragObjectManager = currentCamera.GetComponent<DragObjects>();
        ingameGuiLayout.gameObject.SetActive( false );
        menuGuiLayout.gameObject.SetActive( false );
    }

    private void Start()
    {
        ChangeState( GameState.MAIN_MENU );
    }

    public void StartGame()
    {
        Wave loc_current_wave = Wave.WAVE_TABLE[ currentWaveIndex ];

        characterSpawner.enabled = true;
        characterSpawner.SetCurrentWave( loc_current_wave );

        if ( dayStat != null )
        {
            archivedDayStatTable.Add( dayStat );
        }

        dayStat = new DayStat();
        currentWaveCount = 0;

        foreach ( int loc_wave_info_value in loc_current_wave.waveInfo.Values )
        {
            currentWaveCount += loc_wave_info_value;
        }
    }

    public void CompleteWave()
    {
        ChangeState( GameState.SCORE_SCREEN );
    }

    public void NextWave()
    {
        ++currentWaveIndex;

        if ( currentWaveIndex >= Wave.WAVE_TABLE.Count )
        {
            HandleMessage( StateMessage.GAME_OVER );
        }
        else
        {
            ChangeState( GameState.INGAME );
        }
    }

    private void GameOver()
    {
        archivedDayStatTable.Add( dayStat );
        dayStat = null;
    }

    public int GetWaveIndex()
    {
        return currentWaveIndex + 1;
    }

    public static GameManager GetGameManager()
    {
        return gameManager;
    }

    public Camera GetCamera()
    {
        return currentCamera;
    }

    //public DragObjects GetDragObjectManager()
    //{
    //    return dragObjectManager;
    //}

    public CharacterSpawner GetCharacterSpawner()
    {
        return characterSpawner;
    }

    public int GetCurrentScore()
    {
        int loc_current_score = 0;

        foreach ( DayStat loc_day_stat in archivedDayStatTable )
        {
            int loc_day_score = loc_day_stat.moneyEarned - loc_day_stat.moneyLost;

            loc_current_score += loc_day_score;
        }

        if ( dayStat != null )
        {
            int loc_day_score = dayStat.moneyEarned - dayStat.moneyLost;

            loc_current_score += loc_day_score;
            
        }

        return loc_current_score;
    }

    public DayStat GetDayStat()
    {
        return dayStat;
    }

    public List<DayStat> GetDayStatTable()
    {
        return archivedDayStatTable;
    }

    public void ScoreHeal( Character par_character )
    {
        CanvasInfo loc_canvas_info = GameObject.Instantiate( positiveCanvasInfoPrefab, par_character.transform.position, par_character.transform.rotation );
        int loc_score = Illness.ILLNESS_TABLE[ par_character.GetIllnessType() ].healedScore;
        ++dayStat.healedCount;
        dayStat.moneyEarned += loc_score;

        loc_canvas_info.SetText( string.Format( "+{0}$", loc_score ) );
    }

    public void ScoreLeave( Character par_character )
    {
        ++dayStat.leaverCount;
    }

    public void ScoreDie( Character par_character )
    {
        CanvasInfo loc_canvas_info = GameObject.Instantiate( negativeCanvasInfoPrefab, par_character.transform.position, par_character.transform.rotation );
        int loc_score = Illness.ILLNESS_TABLE[ par_character.GetIllnessType() ].dieScore;
        ++dayStat.diedCount;
        dayStat.moneyLost += loc_score;

        loc_canvas_info.SetText( string.Format( "-{0}$", loc_score ) );
    }

    public void SetSelectedCharacter( Character par_selected_character )
    {
        selectedCharacter = par_selected_character;
    }

    public Character GetSelectedCharacter()
    {
        return selectedCharacter;
    }

    public Transform GetDoctorOffice()
    {
        return doctorOffice;
    }

    public Transform GetHospitalExit()
    {
        return hospitalExit;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        int loc_position_left_count = 0;

        foreach ( WaitingQueue loc_waiting_queue in waitingQueueTable )
        {
            loc_position_left_count += loc_waiting_queue.GetEmptySpaceCount();
        }

        if ( ( GetCharacterSpawner().GetNextCharacter() != null )
             && ( loc_position_left_count > 0 ) )
        {
            Cursor.SetCursor( null, Vector2.zero, CursorMode.ForceSoftware );
        }
        else
        {
            Cursor.SetCursor( waitingCursorTexture, Vector2.zero, CursorMode.ForceSoftware );
        }

        if ( musicAudioSource.enabled
             && !musicAudioSource.isPlaying )
        {
            currentClipIndex = ( currentClipIndex + 1 ) % musicAudioClipTable.Count;
            musicAudioSource.clip = musicAudioClipTable[ currentClipIndex ];
            musicAudioSource.Play();
        }
    }

    public void Register( WaitingQueue par_waiting_queue )
    {
        waitingQueueTable.Add( par_waiting_queue );
    }

    public void ChangeState( GameState par_game_state )
    {
        if ( currentGameState != par_game_state )
        {
            ExitCurrentState();

            EnterNextState( par_game_state );
        }
    }

    private void EnterNextState( GameState par_game_state )
    {
        currentGameState = par_game_state;

        switch ( par_game_state )
        {
            case GameState.MAIN_MENU :
            {
                currentWaveIndex = 0;
                dayStat = null;
                archivedDayStatTable.Clear();
                currentClipIndex = UnityEngine.Random.Range( 0, musicAudioClipTable.Count );
                //musicAudioSource.enabled = false;
                menuGuiLayout.ShowTitleScreen();
                characterSpawner.enabled = false;
                break;
            }
            case GameState.INGAME :
            {
                //musicAudioSource.enabled = true;
                //musicAudioSource.clip = musicAudioClipTable[ UnityEngine.Random.Range( 0, musicAudioClipTable.Count ) ];
                //musicAudioSource.Play();
                characterSpawner.enabled = true;
                ingameGuiLayout.Show();
                StartGame();
                break;
            }
            case GameState.SCORE_SCREEN :
            {
                menuGuiLayout.ShowScoreScreen();
                break;
            }
            case GameState.GAME_OVER :
            {
                menuGuiLayout.ShowGameOverScreen();
                break;
            }
        }
    }

    public void HandleMessage( StateMessage par_message )
    {
        switch ( currentGameState )
        {
            case GameState.MAIN_MENU :
            {
                if ( par_message == StateMessage.ANY_KEY_DOWN )
                {
                    ChangeState( GameState.INGAME );
                }
                break;
            }
            case GameState.INGAME :
            {
                break;
            }
            case GameState.SCORE_SCREEN :
            {
                if ( par_message == StateMessage.ANY_KEY_DOWN )
                {
                    int loc_current_wave_score = dayStat.moneyEarned - dayStat.moneyLost;

                    if ( loc_current_wave_score < 0 )
                    {
                        ChangeState( GameState.MAIN_MENU );
                    }
                    else
                    {
                        NextWave();
                    }
                }

                if ( par_message == StateMessage.GAME_OVER )
                {
                    GameOver();
                    ChangeState( GameState.GAME_OVER );
                    //GameManager.GetGameManager().NextWave();
                }
                break;
            }
            case GameState.GAME_OVER :
            {
                if ( par_message == StateMessage.ANY_KEY_DOWN )
                {
                    ChangeState( GameState.MAIN_MENU );
                }
                break;
            }
        }
    }

    private void ExitCurrentState()
    {
        switch ( currentGameState )
        {
            case GameState.MAIN_MENU :
            {
                menuGuiLayout.HideTitleScreen();
                break;
            }
            case GameState.INGAME :
            {
                ingameGuiLayout.Hide();
                break;
            }
            case GameState.SCORE_SCREEN :
            {
                menuGuiLayout.HideScoreScreen();
                break;
            }
            case GameState.GAME_OVER :
            {
                menuGuiLayout.HideGameOverScreen();
                break;
            }
        }
    }
}