﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class IngameGuiLayout : MonoBehaviour
{
    [SerializeField] private Text waitingCharacterCountText;
    [SerializeField] private Text hoverInfoText;
    [SerializeField] private Text nextPatientInfoText;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text waveIndexText;

    private CharacterSpawner characterSpawner;

    private void Start()
    {
        characterSpawner = GameManager.GetGameManager().GetCharacterSpawner();
    }

    private void Update()
    {
        GameManager loc_game_manager = GameManager.GetGameManager();
        Character loc_next_character = characterSpawner.GetNextCharacter();
        Character loc_selected_character = loc_game_manager.GetSelectedCharacter();

        if ( loc_next_character != null )
        {
            nextPatientInfoText.text = loc_next_character.GetInfo();   
        }
        else
        {
            nextPatientInfoText.text = "Incoming ...";
        }

        waitingCharacterCountText.text = characterSpawner.GetQueueSize().ToString("D2");

        // Compute score
        {
            //GameManager.DayStat loc_day_stat = loc_game_manager.GetDayStat();
            int loc_score = loc_game_manager.GetCurrentScore();

            //if ( loc_day_stat != null )
            //{
            //    loc_score = loc_day_stat.moneyEarned - loc_day_stat.moneyLost;
            //}

            scoreText.text = string.Format( "{0:D6} $$$", loc_score );
        }

        if ( loc_selected_character != null )
        {
            hoverInfoText.text = loc_selected_character.GetInfo();
        }
        else
        {
            hoverInfoText.text = "Waiting ...";
        }

        waveIndexText.text = GameManager.GetGameManager().GetWaveIndex().ToString("D");
    }

    public void Show()
    {
        CanvasGroup loc_ingame_canvas_group = GetComponent<CanvasGroup>();
        gameObject.SetActive(true);
        loc_ingame_canvas_group.alpha = 0;
        loc_ingame_canvas_group.DOFade(1, 2.0f);
    }

    public void Hide()
    {
        CanvasGroup loc_ingame_canvas_group = GetComponent<CanvasGroup>();
        DOTween.Sequence()
            .Append(loc_ingame_canvas_group.DOFade(0, 2.0f))
            .AppendCallback( () => gameObject.SetActive( false ) );
    }
}