﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Character : MonoBehaviour
{
    [SerializeField] private List<Color> characterColorTable;
    [SerializeField] private Image angryIcon;
    [SerializeField] private Canvas happinessCanvas;
    [SerializeField] private Image deadIcon;
    [SerializeField] private Canvas healthCanvas;
    [SerializeField] private float deadBodyDuration = 15.0f;

    private static bool DOES_SHOW_DEBUG_INFO = false;

    private Animator anim;
    public ParticleSystem BloodParticles, HairParticles;
    public GameObject hairObject;
    private float happiness = 1.0f;

    //private Camera currentCamera;
    private QueuePosition queuePosition;
    private NavMeshAgent navMeshAgent;

    private string characterName;
    private int characterAge;
    private float characterHealth;
    private Illness.IllnessType illnessType;
    private bool isInteractable = false;
    private bool isGoingToTheDoctor = false;
    private Transform meshTransform;
    private AudioSource audio;

    private static readonly string[] NAME_TABLE = { "Leslie Wheeldon", "Phil Neild", "Sian Home", "Beryl Rotheram", "Vandira Padua Buzzard", "Becky Rivers", "Heitmeyer Stefan", "Doug Lloyd", "Wilfar Matheson", "Kevin Akinsanya", "Jane Hollings", "Jonathan Hickton", "Claire Pomphrey", "Tricia Wiley", "Miceska Valentina", "Luke Bodo", "Barry Woulfe", "Antonia Robson", "Peter Surgenor", "Silcakesetc London", "Tony Polkey", "Alexandre Torres Filho", "Olufemi Faponmile", "Fraser Purdie", "Neil Clennell", "Hiron Rahman", "Ebrahim Isaacs", "Richard Butcher Tuset", "Rosie Bayley", "Martin Addicott", "Britta Waurich", "Owen Mclaughlin", "Wil Harris", "Ron Wharton", "Shankar Kamalaneson", "Karen Gilgan", "Cosmin Tat", "Henry Hillman", "Suzanne Parsons", "Duryodhan Lanka", "Katy Cooper", "Jenni Disbery", "Darren Doughty", "Williammurry Suit", "Janneke Groeneveld", "Nicola Rossi", "Dave Rudgley", "Teo Terzic", "Alex Stimpson", "Weis Dada", "Lindy Stephens", "Talvinder Sihra", "Charith Roshan", "Rejimol Tom", "Ashleigh Forbes", "Vishal Attrey", "Damian Coates", "Sara Caruana", "Brian Skidmore", "Karli Beare", "Hassan Ehtesham", "Nicky Deugo", "Alistair Kitching", "Mags Mcintyre", "Jovita Didzpetriene", "Saiema Khan", "James Vangucci", "Camilla Hartman", "Alan Serrano", "Frase Fraser", "Karen Ede", "Waring Mcmillan", "Anna Gevers", "Uwazuluonye Uwamanua", "Lorinda Pienaar", "Lucy Skinner", "Denise Chester", "Tomasz Balikowski", "Tristan Blackburn", "Harriet Nkalubo", "Alberto Cipriani", "Avi Maha", "Terence Vieyra", "Richard Hipperson", "Robert Mavronicolas", "Carlg Garner", "Monisha Raju", "James Stanbury", "Tahreen Shad", "Wipada Chotinartrungse", "Paul Krumins", "Fran Fogarty", "Anita Kershaw", "Hannah Pointing", "Gaston Fraser", "Robert Candlin", "Brenda Bayman", "Michelle Watchorn", "Judith Oguaju", "Naveen Joshi", "Rachal Buller", "Mark Kilday", "Sarah Klan", "Rose Pinelli", "Heidi Kennett", "Norman Wallace", "Coreen Anderson", "Ahmad Alrifay", "Eugene David Seo", "Rohit Vaswani", "Yutyu Yut", "Rose Mock", "Margaret Obiora", "John Pacy", "Jennifer Savins", "Paula Atherton", "Graham Neish", "Ankur Consul", "Kalina Jones", "Bhavnidhi Kalra", "Chris R Hartley", "Mariusz Tyczynski", "Moira Hoffman", "Godfery Cassius", "Alison Tatton", "Lesley Sendall", "Lauren Alcroft", "Sue Abrol Sue", "Temit Ope", "Steve Jefferson", "Sally Martyn", "Yvonne Laing", "David Kattan", "Wei Li Teh", "Mark Pluse", "Holly Pennington", "June Mccall", "Jeetandra Patel", "Ankit Shanker", "Rebecca Evison", "Sandra Paige", "Jodi Mullen", "Dread Herbs", "Dolu Abe", "Carol Rocke", "Lewis Pennington", "Maureen Mason", "Niko Tether", "Omotolani Ilori", "Amanda Day", "Joanna Corderoy", "Giacomo Gervasoni", "Ernie Tsao", "Rebecca Bolingbroke", "Tomasz Mis", "Drew Cane", "Paul Sherfield", "Madhavi Krishnani", "Kofi Gottfried", "Rhondda Glasgow", "Gaynor Peers", "Kelly Simpkin", "Pippa Hogan", "Verushka Ramparsad", "Nikolausbonerz Bonerz", "Hayley Thurlow", "Kelly Priest", "Sheila Kleyn", "Jonathan Leakey", "Ady Ryan Ryan", "Laura Dale", "Anna Rooney", "Nathan Hickling", "Jay Bowman", "Tony Charik", "Mike Macintyre", "Gwen Seller", "Tarateanu Vasile", "Abdallah Dawoud", "Martin Clem", "Darron Hillier", "Wakprioro Croffosmorycj", "Maleko Paulo", "Shafiq Qasim", "Spike Sworth", "Saumya Boby", "Zaitouneby Abdoul Hady", "Marcello Venzi", "Jean Stoten", "Sunita Jethwa", "Alfred Frades", "Dean Rigby", "Alexandra Norrish", "Martin Hunt", "Farah Elahi", "Sean Randles", "Kirsten Rose", "Syu Syu", "Anna Bourke", "Luba Pecitova", "Jerry Zielinski", "Gordon Botley", "Rebecca Feeney", "Steve Client London", "Gergana Marinova", "Yousaf Punwar", "Bridget Tyrie", "David Pintor", "Prabha Johar", "Alexis Morley", "Deborah Panther", "Lucy Szymanska", "Pearce Rous", "Gavan Kingdon", "Terry Levy", "Oltreilpolo Oltreilpolo", "Terry Everson", "Mary Melber", "Cornish Elaine", "Teri Garwood", "Amer Durrani", "Adam Tarsh", "Atiq Sadeqzada", "Marion Le Bot", "Janine Yeates", "Graeme Antrobus", "Kay Brain", "Temitope Sadiku", "Kerim Mehmet", "Paul Coode", "Samantha Tonge", "Rubel Rana", "Teresa Hoare", "Kristian Robinson", "Monika Miloszewska", "Tom Tom", "Lior Balul", "Zar Damani", "Aliya Salahuddin", "Kevin Mcculloch", "Dhiraj Kanyal", "John Wormwell", "Sophie Cuvelier", "Jeffrey Biddle", "Peter John Bailey", "Carter Brian", "Ali Abrahams", "Rajinder Pal Bajaj", "Ilona Kotecka", "Abolade Makinde", "Tom Downs", "Aiden Wiffen", "Deirdre Vine", "Graham Croom", "Ben Forgham", "Talwyn Scudi", "Pravin Kulkarni", "Tony Phillips", "Neal Chamberlain", "Jennifer Castello", "Tayden Turner", "Glenda Shuttleworth", "Tracey Scf", "Alberto Branz", "Valia Hatzichristou", "Nosey People", "Jon Greenman", "Firstserveuk London", "Tapiwa Chamboko", "Tony Godber", "Racheal Sharman", "Dave Feery", "Leena Calaca", "Kursh Siddique", "Ralph Tench", "John Osypiw", "Rachel Kemball", "Jan Cushing", "Giorge Gonzalez", "Ikran Ahmed", "Tomasz Likus", "Ndaka Manhambara", "Andrea Kilford", "Yvette Austin", "Howard Tutor", "William Pellowe", "Blair Fisher", "Nichola Legg", "Larry Olaofe", "Dette Walker", "Jose Arturo Navarro", "Adrian Duffen", "Dandy Diwangkara", "Damian Brodie", "Tani Eichmann", "Casmir Igbokwe", "Tessa Lukehurst", "Jaskiran Johal", "Ioannis Orfanos", "Ely Prado" };
    private static readonly int[] AGE_SPREAD_TABLE =
    {
        //0, 0, 0,
        //10, 10, 10, 10,
        20, 20, 20, 20, 20,
        30, 30, 30, 30, 30,
        40, 40, 40, 40, 40, 40, 40,
        50, 50, 50, 50, 50, 50, 50,
        60, 60, 60, 60, 60,
        70, 70, 70, 70,
        80, 80, 80,
        90, 90,
        100
    };

    //private static readonly int[] ILLNESS_SPREAD_TABLE = new[]
    //{
    //    0, 0,
    //    1, 1, 1,
    //    2
    //};

    private void Awake()
    {
        //currentCamera = GameManager.GetGameManager().GetCamera();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void Initialise( Illness.IllnessType par_illness_type )
    {
        const bool DOES_INCLUDE_INACTIVE = true;

        foreach ( SkinnedMeshRenderer loc_mesh_renderer in GetComponentsInChildren<SkinnedMeshRenderer>( DOES_INCLUDE_INACTIVE ) )
        {
            Color color = characterColorTable[ Random.Range( 0, characterColorTable.Count ) ];
            loc_mesh_renderer.material.color = color;
            loc_mesh_renderer.materials[ 2 ].color = color;
        }

        happiness = 1.0f;
        angryIcon.fillAmount = 0.0f;
        deadIcon.fillAmount = 0.0f;
        isGoingToTheDoctor = false;
        navMeshAgent.enabled = true;
        GetComponent<Collider>().enabled = true;
        GetComponent<Rigidbody>().isKinematic = false;
        happinessCanvas.gameObject.SetActive( true );
        healthCanvas.gameObject.SetActive( true );
        anim = GetComponentInChildren<Animator>();
        meshTransform = GetComponentInChildren<Animator>().gameObject.transform;
        audio = GetComponent<AudioSource>();
        gameObject.SetActive( true );
        Deactivate();

        characterName = NAME_TABLE[ Random.Range( 0, NAME_TABLE.Length ) ];

        // Compute age
        {
            int loc_tens = AGE_SPREAD_TABLE[ Random.Range( 0, AGE_SPREAD_TABLE.Length ) ];
            characterAge = loc_tens + Random.Range( 0, 10 );
        }

        // Compute illness
        {
            //int loc_illness_type_index = ILLNESS_SPREAD_TABLE[ Random.Range( 0, ILLNESS_SPREAD_TABLE.Length ) ];

            illnessType = par_illness_type;

            //reset materials and particles
            Material mat = GetComponentInChildren<SkinnedMeshRenderer>().materials[ 2 ];
            mat.color = new Color( mat.color.r, mat.color.g, mat.color.b, 1f );
            BloodParticles.gameObject.SetActive( false );
            HairParticles.gameObject.SetActive( false );
            meshTransform.localScale = Vector3.one;
            hairObject.SetActive( false );

            if ( illnessType == Illness.IllnessType.ARMS_LACK )
            {
                mat.color = new Color( mat.color.r, mat.color.g, mat.color.b, 0f );
                BloodParticles.gameObject.SetActive( true );
            }
            if ( illnessType == Illness.IllnessType.BALDNESS )
            {
                hairObject.SetActive( true );
                HairParticles.gameObject.SetActive( true );
            }
        }

        // Compute health
        {
            characterHealth = Random.Range( 0.5f, 1.0f );
            happiness = Random.Range( 0.5f, 1.0f );
        }
    }

    public string GetInfo()
    {
        StringBuilder loc_string_builder = new StringBuilder();

        loc_string_builder.AppendFormat( "Name : {0}\n", characterName );
        //loc_string_builder.AppendFormat( "Age : {0:D}\n", characterAge );
        loc_string_builder.AppendFormat( "Illness : {0}\n", Illness.ILLNESS_TABLE[ illnessType ].illnessName );
        loc_string_builder.AppendFormat( "Health : {0:P0}", characterHealth );

        return loc_string_builder.ToString();
    }

    public void SetQueuePosition( QueuePosition par_queue_position )
    {
        queuePosition = par_queue_position;
    }

    //public bool IsDragged()
    //{
    //    bool loc_is_dragged = false;
    //    GameObject loc_dragged_object = GameManager.GetGameManager().GetDragObjectManager().GetDraggedObject();

    //    if ( loc_dragged_object != null )
    //    {
    //        loc_is_dragged = loc_dragged_object.GetComponent<Character>() == this;
    //    }

    //    return loc_is_dragged;
    //}

//    private void OnMouseEnter()
//    {
//        if ( isInteractable )
//        {
//            GameManager.GetGameManager().SetSelectedCharacter( this );
//        }
//    }
//
//    private void OnMouseOver()
//    {
//        if ( isInteractable )
//        {
//            GameManager.GetGameManager().SetSelectedCharacter( this );
//        }
//    }
//
//    private void OnMouseExit()
//    {
//        if ( isInteractable )
//        {
//            GameManager.GetGameManager().SetSelectedCharacter( null );
//        }
//    }

    //private void OnMouseUp()
    //{
    //    Ray loc_ray = currentCamera.ScreenPointToRay( Input.mousePosition );
    //    RaycastHit[] loc_raycast_hit_table = Physics.RaycastAll( loc_ray );

    //    foreach ( RaycastHit loc_raycast_hit in loc_raycast_hit_table )
    //    {
    //        QueuePosition loc_queue_position = loc_raycast_hit.collider.GetComponent<QueuePosition>();

    //        if ( loc_queue_position != null )
    //        {
    //            if ( queuePosition != null )
    //            {
    //                queuePosition.SetCurrentCharacter( null );
    //            }

    //            loc_queue_position.GetWaitingQueue().FillPosition( this, loc_queue_position );
    //            break;
    //        }
    //    }
    //}

    private void Update()
    {
        UpdateIcon();

        if ( illnessType == Illness.IllnessType.SHRINKNESS)
        {
            float scale = characterHealth * 0.5f + 0.5f;
            meshTransform.localScale = new Vector3(scale, scale, scale);
        }
        audio.volume = anim.GetFloat("Velocity")*.6f;

        if ( queuePosition != null )
            //&& !IsDragged() )
        {
            Vector3 loc_destination = queuePosition.GetCharacterDock().position; // + Vector3.up * ( navMeshAgent.height / 2.0f );
            //transform.position = loc_destination;
            if ( navMeshAgent.destination != loc_destination )
            {
                navMeshAgent.SetDestination( loc_destination );

                if ( DOES_SHOW_DEBUG_INFO )
                {
                    Debug.Log( string.Format( "current position : <color=blue>{0}</color> - destination : <color=cyan>{1}</color>", transform.position, loc_destination ) );
                    Debug.DrawLine( transform.position, loc_destination );
                }
            }
        }
        happinessCanvas.transform.LookAt( GameManager.GetGameManager().GetCamera().transform.position );
        healthCanvas.transform.LookAt( GameManager.GetGameManager().GetCamera().transform.position );
        if ( characterHealth > 0 )
        {
            anim.SetFloat( "Velocity", ( anim.GetFloat( "Velocity" ) * 3f + navMeshAgent.velocity.magnitude ) * .25f );
        }
        anim.SetFloat( "Health", characterHealth );
    }

    private void FixedUpdate()
    {
        if ( !isGoingToTheDoctor )
        {
            characterHealth = Mathf.Max( 0, characterHealth - Time.deltaTime * Illness.ILLNESS_TABLE[ illnessType ].healthSpeed );
            happiness = Mathf.Max( 0, happiness - Time.deltaTime * Illness.ILLNESS_TABLE[ illnessType ].moodSpeed );

            if ( characterHealth <= 0 )
            {
                Die();
            }
            else if ( happiness <= 0 )
            {
                LeaveHospital();
            }
        }
    }

    private void Die()
    {
        ExitCurrentQueue();
        StartCoroutine( DieCoroutine() );
    }

    private IEnumerator DieCoroutine()
    {
        GameManager loc_game_manager = GameManager.GetGameManager();
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        //GetComponent<Rigidbody>().freezeRotation = false;
//        transform.rotation = Quaternion.Euler( 90, 0, 0 );
        navMeshAgent.enabled = false;
        isGoingToTheDoctor = true; //HACK
        happinessCanvas.gameObject.SetActive( false );
        healthCanvas.gameObject.SetActive( false );

        loc_game_manager.ScoreDie( this );

        yield return new WaitForSeconds( deadBodyDuration );

        loc_game_manager.GetCharacterSpawner().ReturnCharacter( this );
    }

    private void UpdateIcon()
    {
        angryIcon.fillAmount = 1.0f - happiness;
        deadIcon.fillAmount = 1.0f - characterHealth;
    }

    //TODO: set isDragged here ?

    public void Activate()
    {
        isInteractable = true;
    }

    public void Deactivate()
    {
        isInteractable = false;
    }

    public bool HasReachDestination()
    {
        float loc_remaining_distance = navMeshAgent.remainingDistance;
        float loc_stopping_distance = navMeshAgent.stoppingDistance;

        return loc_remaining_distance <= loc_stopping_distance;
    }

    public void ExitCurrentQueue()
    {
        if ( queuePosition != null )
        {
            queuePosition.SetCurrentCharacter( null );
        }
    }

    public void GoToDoctorOffice()
    {
        GameManager loc_game_manager = GameManager.GetGameManager();

        isGoingToTheDoctor = true;
        ExitCurrentQueue();
        loc_game_manager.ScoreHeal( this );
        StartCoroutine( GoToCoroutine( loc_game_manager.GetDoctorOffice().position ) );
    }

    public void LeaveHospital()
    {
        GameManager loc_game_manager = GameManager.GetGameManager();

        isGoingToTheDoctor = true; //HACK
        ExitCurrentQueue();
        loc_game_manager.ScoreLeave(this);
        StartCoroutine( GoToCoroutine( loc_game_manager.GetHospitalExit().position ) );
    }

    private IEnumerator GoToCoroutine( Vector3 par_destination )
    {
        GameManager loc_game_manager = GameManager.GetGameManager();

        navMeshAgent.SetDestination( par_destination );
        
        do
        {
            yield return new WaitForSeconds( 0.1f );
        }
        while ( !HasReachDestination() );

        loc_game_manager.GetCharacterSpawner().ReturnCharacter( this );
    }

    public Illness.IllnessType GetIllnessType()
    {
        return illnessType;
    }
}