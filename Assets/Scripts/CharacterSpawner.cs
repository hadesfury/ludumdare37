﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;

public class CharacterSpawner : WaitingQueue
{
    [SerializeField] private Transform initialSpawnPoint;
    [SerializeField] private Character characterPrefab;
    [SerializeField] private Transform hospitalDoor;

    private readonly List<Character> characterPool = new List<Character>();
    private readonly List<Character> livingCharacter = new List<Character>();
    private float timeSinceLastSpawn = 0.0f;

    private Wave currentWave;
    private int waitingCharacterCount = 0;
    private float spawnInterval = 0;

    private readonly List<Illness.IllnessType> waveIllnessTypeTable = new List<Illness.IllnessType>();
    private int spawnCount = 0;
    private Tween doorTween;

    private void Start()
    {
        foreach ( QueuePosition queue_position in queuePositionTable )
        {
            Collider loc_collider = queue_position.GetComponent<Collider>();

            if ( loc_collider != null )
            {
                GameObject.Destroy( loc_collider );
            }
        }
    }

    public int GetQueueSize()
    {
        int loc_queue_size = waitingCharacterCount;

        foreach ( QueuePosition loc_queue_position in queuePositionTable )
        {
            if ( loc_queue_position.GetCurrentCharacter() != null )
            {
                ++loc_queue_size;
            }
        }

        return loc_queue_size;
    }

    public void ReturnCharacter( Character par_character )
    {
        Assert.IsFalse( characterPool.Contains( par_character ) );
        Assert.IsTrue( livingCharacter.Contains( par_character ) );

        par_character.gameObject.SetActive( false );

        characterPool.Add( par_character );
        livingCharacter.Remove( par_character );

        if ( ( waitingCharacterCount == 0 )
             && ( livingCharacter.Count == 0 ) )
        {
            GameManager.GetGameManager().CompleteWave();
        }
    }

    private Character SpawnNewCharacter()
    {
        Character loc_character_to_spawn = null;
        int loc_illness_type_index = Random.Range( 0, waveIllnessTypeTable.Count );
        Illness.IllnessType loc_illness_type = waveIllnessTypeTable[ loc_illness_type_index ];

        waveIllnessTypeTable.RemoveAt( loc_illness_type_index );

        if ( characterPool.Count > 0 )
        {
            loc_character_to_spawn = characterPool[ 0 ];
            characterPool.RemoveAt( 0 );
        }
        else
        {
            loc_character_to_spawn = GameObject.Instantiate( characterPrefab, initialSpawnPoint.position, initialSpawnPoint.rotation );
        }

        loc_character_to_spawn.transform.position = initialSpawnPoint.position;
        loc_character_to_spawn.transform.rotation = initialSpawnPoint.rotation;
        loc_character_to_spawn.Initialise( loc_illness_type );

        livingCharacter.Add( loc_character_to_spawn );

        return loc_character_to_spawn;
    }

    private void Update()
    {
        timeSinceLastSpawn += Time.deltaTime;

        if ( spawnCount > 0 )
        {
            if ( timeSinceLastSpawn >= spawnInterval )
            {
                ++waitingCharacterCount;
                --spawnCount;

                timeSinceLastSpawn -= spawnInterval;
            }
        }

        Character loc_waiting_character = GetNextCharacter();

        if ( loc_waiting_character != null )
        {
            loc_waiting_character.Activate();
        }

        UpdateQueue();
    }

    protected override void ConcreteQueueUpdate()
    {
        AdvanceInQueueIfPossibleCoroutine();

        QueuePosition loc_last_queue_position = queuePositionTable[ queuePositionTable.Count - 1 ];

        if ( loc_last_queue_position.GetCurrentCharacter() == null )
        {
            if ( waitingCharacterCount > 0 )
            {
                Character loc_new_character = SpawnNewCharacter();

                --waitingCharacterCount;

                loc_last_queue_position.SetCurrentCharacter(loc_new_character);
            }
        }

        if ((doorTween != null)
             && (spawnCount == 0)
             && (loc_last_queue_position.GetCurrentCharacter() == null)
             && (queuePositionTable[queuePositionTable.Count - 2].GetCurrentCharacter() == null))
        {
            doorTween = hospitalDoor.DOLocalRotate(new Vector3(0, 0, 270), 1.0f);
        }
    }

    public Character GetNextCharacter()
    {
        return queuePositionTable[ 0 ].GetCurrentCharacter();
    }

    public void SetCurrentWave( Wave par_current_wave )
    {
        currentWave = par_current_wave;
        waitingCharacterCount = par_current_wave.initialQueueLength;
        spawnInterval = par_current_wave.spawnInteval;
        timeSinceLastSpawn = 0;
        waveIllnessTypeTable.Clear();

        foreach ( KeyValuePair<Illness.IllnessType, int> loc_key_value_pair in par_current_wave.waveInfo )
        {
            for ( int loc_illness_index = 0; loc_illness_index < loc_key_value_pair.Value; loc_illness_index++ )
            {
                waveIllnessTypeTable.Add( loc_key_value_pair.Key );
            }
        }

        spawnCount = waveIllnessTypeTable.Count - waitingCharacterCount;
        doorTween.Kill( false );
        doorTween = hospitalDoor.DOLocalRotate( new Vector3( 0, 0, 160 ), 1.0f );
    }
}