﻿using UnityEngine;

public class SnapTogrid : MonoBehaviour {

    private bool isDragged;

    public float speed = .25f;
    private float x, y, z;
	
    void FixedUpdate ()
    {
        if (!isDragged)
        {
            x = transform.position.x;
//            y = transform.position.y;
            z = transform.position.z;

            if (!Mathf.Approximately(x, Mathf.Round(x)))
                x = Mathf.Lerp(x, Mathf.Round(x), speed);
            else x = Mathf.Round(x);
//            if (y != Mathf.Round(y))
//                y = Mathf.Lerp(y, Mathf.Round(y), speed);
            if (!Mathf.Approximately(z, Mathf.Round(z)))
                z = Mathf.Lerp(z, Mathf.Round(z), speed);
            else z = Mathf.Round(z);
            
            transform.position = new Vector3(x, 0, z);
        }
	}

    public void SetDragged(bool d)
    {
        isDragged = d;
    }
}
