﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MenuGuiLayout : MonoBehaviour
{
    [SerializeField] private RectTransform titleScreenPanel;
    [SerializeField] private RectTransform scoreScreenPanel;
    [SerializeField] private RectTransform gameOverScreenPanel;
    [SerializeField] private RectTransform youLoseTextPanel;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text gameOverScoreText;
    [SerializeField] private Text titleScreenPressAnyKeyText;
    [SerializeField] private Text scoreScreenPressAnyKeyText;
    [SerializeField] private Text gameOverPressAnyKeyText;
    [SerializeField] private AudioSource scoreLineSource;
    [SerializeField] private AudioSource gameOverScoreLineSource;

    private float youLoseTweenDuration = 3.0f;

    private void Awake()
    {
        titleScreenPanel.gameObject.SetActive( false );
        scoreScreenPanel.gameObject.SetActive( false );
        gameOverScreenPanel.gameObject.SetActive( false );
    }

    public void ShowTitleScreen()
    {
        CanvasGroup loc_title_canvas_group = titleScreenPanel.GetComponent<CanvasGroup>();
        gameObject.SetActive( true );
        titleScreenPanel.gameObject.SetActive( true );
        loc_title_canvas_group.alpha = 1;
        titleScreenPanel.transform.localScale = Vector3.zero;
        titleScreenPressAnyKeyText.gameObject.SetActive( false );

        DOTween.Sequence()
            .Append( titleScreenPanel.transform.DOScale( Vector3.one, 3.0f ).SetEase( Ease.InOutBounce ) )
            .AppendCallback( () => titleScreenPressAnyKeyText.gameObject.SetActive( true ) );
        DOTween.Sequence()
            .AppendInterval( 2.0f )
            .Append( GameManager.GetGameManager().GetCamera().DOShakePosition( 1.5f, 0.3f, 9, 0.0f ) );
    }

    public void HideTitleScreen()
    {
        CanvasGroup loc_title_canvas_group = titleScreenPanel.GetComponent<CanvasGroup>();
        titleScreenPressAnyKeyText.gameObject.SetActive( false );
        DOTween.Sequence()
            .Append( loc_title_canvas_group.DOFade( 0, 2.0f ) )
            .AppendCallback( () => titleScreenPanel.gameObject.SetActive( false ) );
    }

    public void ShowScoreScreen()
    {
        CanvasGroup loc_score_canvas_group = scoreScreenPanel.GetComponent<CanvasGroup>();
        gameObject.SetActive( true );
        youLoseTextPanel.gameObject.SetActive( false );
        scoreScreenPanel.gameObject.SetActive( true );
        loc_score_canvas_group.alpha = 0;
        loc_score_canvas_group.DOFade( 1, 2.0f );
        scoreScreenPressAnyKeyText.gameObject.SetActive( false );

        StartCoroutine( DisplayScoreCoroutine() );
    }

    public void HideScoreScreen()
    {
        CanvasGroup loc_score_canvas_group = scoreScreenPanel.GetComponent<CanvasGroup>();
        scoreScreenPressAnyKeyText.gameObject.SetActive( false );
        DOTween.Sequence()
            .Append( loc_score_canvas_group.DOFade( 0, 2.0f ) )
            .AppendCallback( () => scoreScreenPanel.gameObject.SetActive( false ) );
    }

    public void ShowGameOverScreen()
    {
        CanvasGroup loc_game_over_canvas_group = gameOverScreenPanel.GetComponent<CanvasGroup>();
        gameObject.SetActive( true );
        gameOverScreenPanel.gameObject.SetActive( true );
        loc_game_over_canvas_group.alpha = 0;
        loc_game_over_canvas_group.DOFade( 1, 2.0f );
        gameOverPressAnyKeyText.gameObject.SetActive( false );

        StartCoroutine( DisplayGameOverCoroutine() );
    }

    public void HideGameOverScreen()
    {
        CanvasGroup loc_game_over_canvas_group = gameOverScreenPanel.GetComponent<CanvasGroup>();
        gameOverPressAnyKeyText.gameObject.SetActive( false );
        DOTween.Sequence()
            .Append( loc_game_over_canvas_group.DOFade( 0, 2.0f ) )
            .AppendCallback( () => gameOverScreenPanel.gameObject.SetActive( false ) );
    }

    private IEnumerator DisplayGameOverCoroutine()
    {
        List<GameManager.DayStat> loc_day_stat_table = GameManager.GetGameManager().GetDayStatTable();
        StringBuilder loc_score_string_builder = new StringBuilder();
        int loc_wave_index = 0;
        float loc_time_between_text_lines = 0.33f;
        int loc_total_score = 0;

        gameOverScoreText.text = "";
        yield return new WaitForSeconds( 2.0f );

        foreach ( GameManager.DayStat loc_day_stat in loc_day_stat_table )
        {
            int loc_wave_score = loc_day_stat.moneyEarned - loc_day_stat.moneyLost;

            loc_score_string_builder.AppendFormat( "Wave {0} : {1}$\n", loc_wave_index + 1, loc_wave_score );
            gameOverScoreText.text = loc_score_string_builder.ToString();
            gameOverScoreLineSource.Play();
            loc_total_score += loc_wave_score;

            yield return new WaitForSeconds( loc_time_between_text_lines );

            ++loc_wave_index;
        }
        loc_score_string_builder.AppendFormat( "Total earned (Highscore) : {0}$", loc_total_score );
        yield return new WaitForSeconds( loc_time_between_text_lines );
        gameOverScoreText.text = loc_score_string_builder.ToString();
        gameOverScoreLineSource.Play();

        gameOverPressAnyKeyText.gameObject.SetActive( true );
    }

    private IEnumerator DisplayScoreCoroutine()
    {
        StringBuilder loc_score_string_builder = new StringBuilder();
        GameManager.DayStat loc_day_stat = GameManager.GetGameManager().GetDayStat();
        List<GameManager.DayStat> loc_day_stat_table = GameManager.GetGameManager().GetDayStatTable();
        int loc_initial_score = 0;
        float loc_time_between_text_lines = 0.33f;

        foreach ( GameManager.DayStat loc_stat in loc_day_stat_table )
        {
            int loc_day_score = loc_stat.moneyEarned - loc_stat.moneyLost;

            loc_initial_score += loc_day_score;
        }

        scoreText.text = "";

        yield return new WaitForSeconds(loc_time_between_text_lines);

        loc_score_string_builder.AppendFormat( "Initial money : {0}\n", loc_initial_score );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( 2.0f );

        loc_score_string_builder.AppendFormat( "Saved people : {0}\n", loc_day_stat.healedCount );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        loc_score_string_builder.AppendFormat( "Money earned : {0}$\n", loc_day_stat.moneyEarned );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        loc_score_string_builder.AppendFormat( "Bored people : {0}\n", loc_day_stat.leaverCount );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        loc_score_string_builder.AppendFormat( "Killed people : {0}\n", loc_day_stat.diedCount );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        loc_score_string_builder.AppendFormat( "Money lost : {0}$\n", loc_day_stat.moneyLost );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        int loc_score = loc_day_stat.moneyEarned - loc_day_stat.moneyLost + loc_initial_score;

        loc_score_string_builder.AppendFormat( "Final money : {0}$\n", loc_score );

        scoreText.text = loc_score_string_builder.ToString();
        scoreLineSource.Play();

        yield return new WaitForSeconds( loc_time_between_text_lines );

        if ( loc_score < 0 )
        {
            youLoseTextPanel.gameObject.SetActive( true );
            youLoseTextPanel.localScale = Vector3.zero;
            youLoseTextPanel.transform.DOScale( Vector3.one, youLoseTweenDuration ).SetEase( Ease.InOutBounce );
            DOTween.Sequence()
                .AppendInterval( 2.0f )
                .Append( GameManager.GetGameManager().GetCamera().DOShakePosition( 1.5f, 0.3f, 9, 0.0f ) );
            yield return new WaitForSeconds( youLoseTweenDuration );
        }

        scoreScreenPressAnyKeyText.gameObject.SetActive( true );
    }

    private void Update()
    {
        if ( ( titleScreenPressAnyKeyText.gameObject.activeInHierarchy || scoreScreenPressAnyKeyText.gameObject.activeInHierarchy || gameOverPressAnyKeyText.gameObject.activeInHierarchy )
             && Input.anyKeyDown )
        {
            GameManager.GetGameManager().HandleMessage( GameManager.StateMessage.ANY_KEY_DOWN );
        }
    }
}