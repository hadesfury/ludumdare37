﻿using System;
using UnityEngine;

public class QueuePosition : MonoBehaviour
{
    [SerializeField] private Color highlighetColor = Color.green;
    [SerializeField] private Transform characterDock;
    [SerializeField] private MeshRenderer meshRenderer;

    private static bool DOES_SHOW_DEBUG_INFO = false;

    private WaitingQueue waitingQueue;
    private Color initialColor;
    private Character currentCharacter;
    private int queuePositionIndex;

    private void Start()
    {
        initialColor = meshRenderer.material.color;
    }

    private void OnMouseUpAsButton()
    {
        Character loc_next_character = GameManager.GetGameManager().GetCharacterSpawner().GetNextCharacter();

        if ( ( loc_next_character != null )
             && waitingQueue.IsSpaceAvailable( this ) )
        {
            waitingQueue.FillPosition( loc_next_character, this );
        }
    }

    private void OnMouseEnter()
    {
        HighlightPosition();
    }

    private void OnMouseOver()
    {
        Character loc_next_character = GameManager.GetGameManager().GetCharacterSpawner().GetNextCharacter();

        if ((loc_next_character != null)
             && waitingQueue.IsSpaceAvailable(this))
        {
            HighlightPosition();
        }
        else
        {
            ClearHighlight();
        }
    }

    private void OnMouseExit()
    {
        ClearHighlight();
    }

    public WaitingQueue GetWaitingQueue()
    {
        return waitingQueue;
    }

    private void HighlightPosition()
    {
        Character loc_next_character = GameManager.GetGameManager().GetCharacterSpawner().GetNextCharacter();

        if ( ( loc_next_character != null )
             && waitingQueue.IsSpaceAvailable( this ) )
        {
            meshRenderer.material.color = highlighetColor;
            meshRenderer.material.SetColor("_EmissionColor", highlighetColor*.1f);
        }
    }

    private void ClearHighlight()
    {
        meshRenderer.material.color = initialColor;
        meshRenderer.material.SetColor("_EmissionColor", Color.black);
    }

    //private Character GetDraggedCharacter()
    //{
    //    Character loc_dragged_character = null;
    //    DragObjects loc_drag_object_manager = waitingQueue.GetDragObjectManager();
    //    GameObject loc_dragged_object = loc_drag_object_manager.GetDraggedObject();

    //    if ( loc_dragged_object != null )
    //    {
    //        loc_dragged_character = loc_dragged_object.GetComponent<Character>();
    //    }

    //    return loc_dragged_character;
    //}

    public void SetWaitingQueue( WaitingQueue par_waiting_queue, int par_queue_position_index )
    {
        waitingQueue = par_waiting_queue;
        queuePositionIndex = par_queue_position_index;
    }

    public Character GetCurrentCharacter()
    {
        return currentCharacter;
    }

    public void SetCurrentCharacter( Character par_character )
    {
        if ( currentCharacter != null )
        {
            currentCharacter.transform.SetParent( null );
            currentCharacter.SetQueuePosition( null );

            if ( DOES_SHOW_DEBUG_INFO )
            {
                Debug.Log( string.Format( "Character {0} leaving position {1}", currentCharacter.name, queuePositionIndex ) );
            }
        }

        currentCharacter = par_character;

        if ( currentCharacter != null )
        {
            currentCharacter.ExitCurrentQueue();
            currentCharacter.transform.SetParent( characterDock, true );
            //currentCharacter.transform.localPosition = Vector3.zero;
            currentCharacter.SetQueuePosition( this );

            if ( DOES_SHOW_DEBUG_INFO )
            {
                Debug.Log( string.Format( "Character {0} is moving to position {1}", currentCharacter.name, queuePositionIndex ) );
            }
        }
    }

    public Transform GetCharacterDock()
    {
        return characterDock;
    }
}