﻿using System.Collections.Generic;

public class Wave
{
    //public float speedFactor;
    public float spawnInteval;
    public int initialQueueLength;
    public Dictionary<Illness.IllnessType, int> waveInfo;

    public static readonly List<Wave> WAVE_TABLE = new List<Wave>()
    {
        new Wave()
        {
            waveInfo = new Dictionary<Illness.IllnessType, int>()
            {
                { Illness.IllnessType.SHRINKNESS, 3 },
                { Illness.IllnessType.BALDNESS, 5 },
                { Illness.IllnessType.ARMS_LACK, 2}
            },
            spawnInteval = 3.5f,
            //speedFactor = 1.0f,
            initialQueueLength = 2
        },
        new Wave()
        {
            waveInfo = new Dictionary<Illness.IllnessType, int>()
            {
                { Illness.IllnessType.SHRINKNESS, 6 },
                { Illness.IllnessType.BALDNESS, 10 },
                { Illness.IllnessType.ARMS_LACK, 4 }
            },
            spawnInteval = 3f,
            //speedFactor = 2.0f,
            initialQueueLength = 4
        }/*,
        new Wave()
        {
            waveInfo = new Dictionary<Illness.IllnessType, int>()
            {
                { Illness.IllnessType.SHRINKNESS, 10 },
                { Illness.IllnessType.BALDNESS, 15 },
                { Illness.IllnessType.ARMS_LACK, 8 }
            },
            spawnInteval = 2.5f,
            //speedFactor = 3.0f,
            initialQueueLength = 6
        }*/
    };
}